import styled from 'styled-components';
import React from 'react';

const StyledBox = styled.div`
  position: absolute;
  top: 10%;
  left: 50%;
  transform: translate(-50%, -50%);
  
  :before {
    content: '*';
    position: absolute;
    top: 0;
    right: 0;
    width: 20%;
    height: 100%;
    text-align: center;
    font-size: 28px;
    line-height: 45px;
    color: rgba(255, 255, 255, 0.5);
    background-color: rgba(255, 255, 255, 0.1);
    pointer-events: none;
  }
  
  :hover::before {
    color: rgba(255, 255, 255, 0.6);
    background-color: rgba(255, 255, 255, 0.2);
  }
`
const StyledSelect = styled.select`
  background-color: #0563af;
  color: white;
  padding: 12px;
  width: 250px;
  border: none;
  font-size: 20px;
  box-shadow: 0 5px 25px rgba(0, 0, 0, 0.2);
  appearance: button;
  outline: none;
  option {
    padding: 30px;
  }
`

function Select(props){

  let item = [];

  for(let i = 0; i < props.options; i++ ){
    item.push(<option key={props.data[i]} value={props.data[i]}>{props.data[i]}</option>)
  }
  return(
    <StyledBox>
      <StyledSelect onChange={props.onChange}>
        {item}
      </StyledSelect>
    </StyledBox>
  )
}
export default Select;