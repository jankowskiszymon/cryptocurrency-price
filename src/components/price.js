import React, {Component} from 'react';
import Select from './select';
import DisplayPrice from './displayPrice';
import Footer from './footer';

class Price extends Component{
    constructor(props){
        super(props);
        this.state = {
            pricesUSD: [],
            pricesEUR: [],
            pricesGBP: [],
            prices: [],
            data: ['USD','EUR','GBP'],
            value: 'USD',
            time: '',
            numberOfOption: '3',
            author: 'Szymon Jankowski'
        };    
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        this.setState({value: e.target.value})        
    }


    componentDidMount() {
        fetch('http://localhost:9000/prices')
          .then(response => response.json())
          .then(json => {
            this.setState({
                pricesUSD: Object.keys(json.prices.usd).map(key =>[String(key), json.prices.usd[key]]),
                pricesEUR: Object.keys(json.prices.eur).map(key =>[String(key), json.prices.eur[key]]),
                pricesGBP: Object.keys(json.prices.gbp).map(key =>[String(key), json.prices.gbp[key]]),
                time: new Date().toLocaleDateString() + ' | ' + new Date().toLocaleTimeString()
             })
             console.log(this.state.pricesUSD) 
          });
      }

      data(data){
        switch(data){
            case 'USD' : return this.state.pricesUSD 
            case 'EUR' : return this.state.pricesEUR
            case 'GBP' : return this.state.pricesGBP
            default: return null
        } 
     }

    render() {
        return(
            <div>
                <Select options={this.state.numberOfOption} 
                        data={this.state.data} 
                        value={this.state.value} 
                        onChange={this.handleChange}/>
                <DisplayPrice data={this.data(this.state.value)} currency={this.state.value} value={this.state.value}/>
                <Footer author={this.state.author}/>
            </div>
        )    
    }   
}

export default Price;