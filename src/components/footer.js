import React from 'react';
import styled from 'styled-components';

const Div = styled.div`
    position: fixed;
    height: 20px;
    width: 100%;
    bottom: 0;
    left: 0;
    background: #0563af;
    text-align: center;
    color: white;
    font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; 
`

const Footer = ({author}) => (
    <Div>Made with love by {author}. GNU GENERAL PUBLIC LICENSE</Div>
)

export default Footer;
