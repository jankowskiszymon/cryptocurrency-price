import React from 'react';
import styled from 'styled-components';

const FlexBox = styled.div`
    display: flex;
    justify-content: space-around;
    position: absolute;
    top: 30%;
    left: 50%;
    transform: translate(-50%, -50%);
    color: #0563af;
    @media (max-width: 600px){
        display: block;
        position: absolute;
        top: 30%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 20px;
        margin-top: 30px;
    }
`
const StyledDiv = styled.div`
    width: 300px;
    height: 150px;
    margin-left: 30px;
    border: 1px solid white;
    border-radius: 20px;
    background-color: white;
    padding: 20px;
    box-sizing: border-box;
    @media (max-width: 600px) {
        width: 150px;
        height: 90px;
        margin-top: 20px;
        margin: 0 auto;
        box-sizing: border-box;
    }
`
const StyledTitle = styled.div`
    font-size: 15px;
    color: #004882;
`
const StyledPrice = styled.div`
    font-size: 55px;
    margin-top: 10px;
    span {
        font-size: 25px;
    }
    @media (max-width: 600px) {
        font-size: 20px;
        margin-top: 10px;
        span {
            font-size: 10px;
        }
    }
`   

function format(str,cut){
    return `${str.substring(0,cut)}`;
}

function Box(props){

    return(
        <StyledDiv>
            <StyledTitle>
                {props.crypto.toUpperCase()}   
            </StyledTitle>
            <StyledPrice>
                {props.price} <span>{(props.currency === 'USD' ? '$' : ((props.currency === 'EUR') ? '€' : '£'))}</span>
            </StyledPrice> 
        </StyledDiv>
    )
}

function DisplayPrice(props){
    
    const item = [];
    
    for(let i = 0; i < 3; i++){
        if (props.data[i]){        
            item.push(<Box key={`box ${i}`} price={format(props.data[i][1],8)} crypto={props.data[i][0]} currency={props.value}/>)
        }   
    }
    return(
        <FlexBox>
            {item}
        </FlexBox>
    )
}

export default DisplayPrice;