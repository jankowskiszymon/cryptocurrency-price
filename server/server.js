const express = require('express');
const cors = require('cors');
const fetch = require('node-fetch');

const app = express();


const port = 9000;

let price = 0;

const getPrice = () => {
    let http = 'https://api.coindesk.com/v1/bpi/currentprice.json';

    fetch(http)
    .then(res => res.json())
    .then(data => price = data)
    .catch(err => console.log(err))
}

app.use(cors())
getPrice();
app.get('/prices', (req,res,next) => res.json({
    'prices': {
    'usd': {
        'bitcoin': price.bpi.USD.rate,
        'ethereum': '183.232',
        'litecoin': '101.312'
        },
    'eur': {
        'bitcoin': price.bpi.EUR.rate,
        'ethereum': '143.321',
        'litecoin': '98.142'
        },
    'gbp': {
        'bitcoin': price.bpi.GBP.rate,
        'ethereum': '130.232',
        'litecoin': '89.232'
    }}}));


app.get('/api', (req,res) => res.send(data))


app.listen(port, () => console.log(`Listening on port ${port}`))