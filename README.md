This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm i`

Install the packages I use

### `cd server`
### `npm i cors`
### `node server.js` 
### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
Open [http://localhost:9000/prices](http://localhost:9000/prices) to view serven in the browser.

